package lab01;
import java.lang.Math;
import java.util.Scanner;

public class Quadratic_equation_2 {
	
	public static void PTB2(float a , float b , float c) {
		float delta= a*a-4*b*c;
		if(a==0 && b==0 && c==0 ) {
			System.out.println("Phuong trinh vo so nghiem");
		}
		if(delta>0) {
			System.out.println("Phuong trinh co nghiem thu nhat la x1= "+((-b) +Math.sqrt(delta))/(2*a));
			System.out.println("Phuong trinh co nghiem thu nhat la x2= "+((-b) -Math.sqrt(delta))/(2*a));
		}else if(delta==0) {
			System.out.println("Phuong trinh co hai nghiem trung nhau : "+ ((-b)/(2*a)));
		}else if(delta<0) {
			System.out.println("Phuong trinh vo nghiem");
		}
	}
	 public static void main(String args[]) {
		 float a,b,c;
		 Scanner scanner = new Scanner(System.in);
		 System.out.println("Nhap vao bien a: ");
		 a=scanner.nextFloat();
		 System.out.println("Nhap vao bien b: ");
		 b=scanner.nextFloat();
		 System.out.println("Nhap vao bien c: ");
		 c=scanner.nextFloat();
		 PTB2(a,b,c);
	
	}

}
