package hust.soict.hedspi.aims.media;

public abstract class Media {
	protected String title;
    protected String category;
    protected float cost;
	public Media() {
		// TODO Auto-generated constructor stub
	}
	public Media(String title) {
		this.title =title;
	}
	public Media(String title,String category) {
		this(title);
		this.category=category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public float getCost() {
		return cost;
	}
	

    
}
