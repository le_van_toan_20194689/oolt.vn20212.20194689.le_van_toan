package hust.soict.hedspi.aims.media;

public class Disc extends Media {
       protected String director;
       protected int length;
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public Disc(String director, int length) {
		this.director = director;
		this.length = length;
	}
	public Disc() {
		super();
	}
       
	

}
