package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.List;

public class CompactDisc extends Disc implements Playable {
       private String artist;
       private ArrayList<Track> tracks = new ArrayList <Track>();
	
	public CompactDisc(String artist, ArrayList<Track> tracks) {
		super();
		this.artist = artist;
		this.tracks = tracks;
	}
	public CompactDisc() {
		super();
	}
	public int getLength (ArrayList<Track> tracks) {
		int sumLength =0;
				for(int i=0; i < tracks.size();i++) {
					sumLength += tracks.get(i).getLength();
				}
		super.length =sumLength;
		return super.length;
	}
    public void addTrack(Track track){
    	if(tracks.contains(track)) {
    		System.out.println("Alredy exist");
    	}
    	else tracks.add(track);
    }
    public void removeTrack(Track track) {
    	if(tracks.contains(track)) {
    		tracks.remove(track);
    	}else System.out.println("Do not exist");
    }
// overide play
    public void play() {
    	for(int i=0;i<this.tracks.size();i++) {
    		this.tracks.get(i).play();
    	}
    }

}
