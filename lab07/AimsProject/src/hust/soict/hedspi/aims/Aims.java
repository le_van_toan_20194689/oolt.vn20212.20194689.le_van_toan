package hust.soict.hedspi.aims;
import java.util.Scanner;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.order.Order;
import hust.soict.hedspi.aims.utils.MyDate;

public class Aims {
	public static void showMenu() {
		System.out.println("Order Management Application");
		System.out.println("------------------------");
		System.out.println("1.Create new order");
		System.out.println("2.Add item to the order");
		System.out.println("3.Delete item to the order");
		System.out.println("4.Display the items list of order");
		System.out.println("0.Exit");
		System.out.println("------------------------");
		System.out.println("Plese choose a number:0-1-2-3-4");
	}

	public static void main(String[] args) {    
        Order OrderOne = null;
        Media MediaItem;
        DigitalVideoDisc DVDItem = null;
        Book BookItem = null;
        CompactDisc CDItem=null;
        int Mediachoose = 0;
        do {
        	 Aims.showMenu();
        	 int choose;
             int count=0;
             Scanner sc= new Scanner(System.in);
             do {
    			System.out.println("Plese choose some case: ");
    			choose=sc.nextInt();
             }while (choose<0||choose>4);
        	 switch (choose) {
      		case 1: {
      		     OrderOne =new Order();
      		   
      		    System.out.println("Ban da tao mot order moi");
      		    break;
      		}
      		case 2:{
      			System.out.println("------------------");
      		    System.out.println("Chon DVD hoac Book");
      		    System.out.println("1.DVD");
      		    System.out.println("2.Book");
      		    System.out.println("3.CD");
      		    System.out.println("------------------");
      		    int chooseItem;
      		    Scanner sc1=new Scanner(System.in);
      		    chooseItem=sc1.nextInt();
      		    if(chooseItem==1) Mediachoose =1; 
      		    if(chooseItem==2) Mediachoose =2;
      		    if(chooseItem==3) Mediachoose =3;
      		    switch (chooseItem) {
				case 1: {
					BookItem =new Book();
					String BookName;
					System.out.println("Nhap vao ten sach ban muon mua:");
					Scanner sc2=new Scanner(System.in);
					BookName=sc2.nextLine();
				    BookItem.setTitle(BookName);
//				    Do ko co data base nen em set cac du lieu cua Book la default
				    break;
				}
				case 2:{
					DVDItem =new DigitalVideoDisc();
					String DVDName;
					System.out.println("Nhap vao ten DVD ban muon mua:");
					Scanner sc3=new Scanner(System.in);
					DVDName=sc3.nextLine();
				    DVDItem.setTitle(DVDName);
//				    Do ko co data base nen em set cac du lieu cua Book la default
				    break;
				}
				case 3:{
					CDItem =new CompactDisc();
					String CDName;
					System.out.println("Nhap vao ten CD ban muon mua:");
					Scanner sc3=new Scanner(System.in);
					CDName=sc3.nextLine();
				    CDItem.setTitle(CDName);
//				    Do ko co data base nen em set cac du lieu cua Book la default
				}
				default:
					break;
				}
      		    if(Mediachoose ==1) OrderOne.addMedia(BookItem);
    			if(Mediachoose==2) OrderOne.addMedia(DVDItem);
    			if(Mediachoose==3) OrderOne.addMedia(CDItem);
    			System.out.println("Ban da them thanh cong vao gio hang");
      		    break;
      		}
      		case 3:{
      			if(Mediachoose ==1) OrderOne.removeMedia(BookItem);
    			if(Mediachoose==2) OrderOne.removeMedia(DVDItem);
    			if(Mediachoose==3) OrderOne.removeMedia(CDItem);
      			break;
      		}
      		case 4:{
      			OrderOne.printOrders();
      			break;
      		}
      		case 0:{
      			return;
      		}
      		default:
      			throw new IllegalArgumentException(" " + choose);
      		}
		} while (true);
      
         
	}

}
