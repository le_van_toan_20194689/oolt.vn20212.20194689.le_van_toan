import java.lang.runtime.SwitchBootstraps;
import java.sql.Date;
import java.util.Calendar;
import java.util.Scanner;

public class MyDate {
	static String[] months = {
			"January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December",
			"Jan.", "Feb.", "Mar.", "Apr.", "May", "June",
			"July", "Aug.", "Sept.", "Oct.", "Nov.", "Dec.",
			"Jan", "Feb", "Mar", "Apr", "May", "Jun",
			"Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
			"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
	static int[] days = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	private int day, month, year;
    
	static boolean isLeapYear(int year) {
		if (year % 4 == 0 && !(year % 100 == 0 && year % 400 != 0)) {
			return true;
		}
		return false;
	}
	
	public MyDate() {
		Calendar calendar = Calendar.getInstance();
		this.day = calendar.get(Calendar.DAY_OF_MONTH);
		this.month = calendar.get(Calendar.MONTH) + 1;
		this.year = calendar.get(Calendar.YEAR);
	}
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String str) {
		int tMonth = -1, pMonth = 0, 
			tYear = -1, pYear = 0,
			tDay = -1, pDay = 0;
		
		// Lấy ra tháng
		for (int i=0; i<months.length; i++) {
			tMonth = str.indexOf(months[i]);
			pMonth = tMonth + months[i].length() - 1;
			if (tMonth >= 0 
			&& (tMonth == 0 || !Character.isDigit(str.charAt(tMonth-1) ) )
			&& (pMonth == str.length() - 1 || !Character.isDigit(str.charAt(pMonth+1)) )) {
				this.month = i % 12 + 1;
				break;
			} else {
				tMonth = -1;
			}
		}
		if (tMonth == -1) {
			System.out.println("Sai định dạng tháng");
			return;
		}
		
		// Lấy ra năm
		for (int j=0; j<str.length()-3; j++) {
			if ( j + 3 < tMonth || pMonth < j ) {
				try {
					this.year = Integer.valueOf(str.substring(j, j+4));
					tYear = j;
					pYear = j+3;
					break;
				} catch (NumberFormatException e) {}
			}
		}
		if (tYear == -1) {
			System.out.println("Sai định dạng năm");
			return;
		}
		
		// Lấy ra ngày
		for (int j=0; j<str.length()-1; j++) {
			if ( (j + 1 < tMonth || pMonth < j) && (j + 1 < tYear || pYear < j) ) {
				try {
					this.day = Integer.valueOf(str.substring(j, j+2));
					tDay = j;
					pDay = j+1;
					break;
				} catch (NumberFormatException e) {}
			}
		}
		if (tDay == -1) {
			for (int j=0; j<str.length(); j++) {
				if ( (j < tMonth || pMonth < j) && (j < tYear || pYear < j) ) {
					try {
						this.day = Integer.valueOf(str.substring(j, j+1));
						tDay = j;
						pDay = j;
						break;
					} catch (NumberFormatException e) {}
				}
			}
			if (tDay == -1) {
				System.out.println("Sai định dạng ngày");
				return;
			}
		}
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		int maxDay = days[this.month - 1];
		
		if (isLeapYear(this.year) && this.month == 2) {
			maxDay = 29;
		}
		if (day <= 0 || day > maxDay) {
			System.out.println("Ngày không hợp lệ");
			return;
		}
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if (month <= 0 || 12 < month) {
			System.out.println("Tháng không hợp lệ");
			return;
		}
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public void accept() {
		System.out.println("Nhap ngay, thang, nam:");
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		
		int tMonth = -1, pMonth = 0, 
				tYear = -1, pYear = 0,
				tDay = -1, pDay = 0;
			
			// Lấy ra tháng
			for (int i=0; i<months.length; i++) {
				tMonth = str.indexOf(months[i]);
				pMonth = tMonth + months[i].length() - 1;
				if (tMonth >= 0 
				&& (tMonth == 0 || !Character.isDigit(str.charAt(tMonth-1) ) )
				&& (pMonth == str.length() - 1 || !Character.isDigit(str.charAt(pMonth+1)) )) {
					this.month = i % 12 + 1;
					break;
				} else {
					tMonth = -1;
				}
			}
			if (tMonth == -1) {
				System.out.println("Sai định dạng tháng");
				return;
			}
			
			// Lấy ra năm
			for (int j=0; j<str.length()-3; j++) {
				if ( j + 3 < tMonth || pMonth < j ) {
					try {
						this.year = Integer.valueOf(str.substring(j, j+4));
						tYear = j;
						pYear = j+3;
						break;
					} catch (NumberFormatException e) {}
				}
			}
			if (tYear == -1) {
				System.out.println("Sai định dạng năm");
				return;
			}
			
			// Lấy ra ngày
			for (int j=0; j<str.length()-1; j++) {
				if ( (j + 1 < tMonth || pMonth < j) && (j + 1 < tYear || pYear < j) ) {
					try {
						this.day = Integer.valueOf(str.substring(j, j+2));
						tDay = j;
						pDay = j+1;
						break;
					} catch (NumberFormatException e) {}
				}
			}
			if (tDay == -1) {
				for (int j=0; j<str.length(); j++) {
					if ( (j < tMonth || pMonth < j) && (j < tYear || pYear < j) ) {
						try {
							this.day = Integer.valueOf(str.substring(j, j+1));
							tDay = j;
							pDay = j;
							break;
						} catch (NumberFormatException e) {}
					}
				}
				if (tDay == -1) {
					System.out.println("Sai định dạng ngày");
					return;
				}
			}
	}
	public void print() {
		String monthStr =new String();
		switch (this.month){
		case 1: 
			monthStr="January";
			break;	
		case 2: 
			monthStr="Febrary";
			break;
				
		case 3:
			monthStr="March";
			break;
		case 4:
			monthStr="April";
			break;
		case 5:
			monthStr="May";
			break;
		case 6:
			monthStr="June";
			break;
		case 7:
			monthStr="July";
			break;
		case 8:
			monthStr="August";
			break;
		case 9:
			monthStr="September";
			break;
		case 10:
			monthStr="October";
			break;
		case 11:
			monthStr="November";
			break;
		case 12:
			monthStr="December";
			break;
		default:			
			throw new IllegalArgumentException("Unexpected value: " + this.month);
		}
		System.out.println(""+monthStr+" "+this.day+"th"+" "+this.year);
	}
	
public void PrintByYourSelf() {
	String monthStr =new String();
	switch (this.month){
	case 1: 
		monthStr="January";
		break;	
	case 2: 
		monthStr="Febrary";
		break;
			
	case 3:
		monthStr="March";
		break;
	case 4:
		monthStr="April";
		break;
	case 5:
		monthStr="May";
		break;
	case 6:
		monthStr="June";
		break;
	case 7:
		monthStr="July";
		break;
	case 8:
		monthStr="August";
		break;
	case 9:
		monthStr="September";
		break;
	case 10:
		monthStr="October";
		break;
	case 11:
		monthStr="November";
		break;
	case 12:
		monthStr="December";
		break;
	default:			
		throw new IllegalArgumentException("Unexpected value: " + this.month);
	}
	
	System.out.println("1.yyyy-MM-dd");
	System.out.println("2.d/M/yyyy");
	System.out.println("3.dd-MMM-yyyy");
	System.out.println("4.MMM d yyyy");
	System.out.println("5.mm-dd-yyyy");
	System.out.println("Nhap vao dinh dang ngay thang nam ma ban muon in: ");
	
	
    Scanner sc= new Scanner(System.in);
    int a;
    a=sc.nextInt();
    
    switch (a) {
	case 1: 
		System.out.println(""+this.year+"-"+this.month+"-"+this.day);
		break;
	case  2:
		System.out.println(""+this.day+"/"+this.month+"/"+this.year);
		break;
	case 3:
		System.out.println(""+this.day+"-"+monthStr+"-"+this.year);
		break;
	case 4:
		System.out.println(""+monthStr+" " +this.day+" "+ this.year);
		break;
	case 5:
		System.out.println(""+this.day+"-"+this.month+"-"+this.year);
		break;
	
	default:
		throw new IllegalArgumentException("Unexpected value: " + a);
	}
    
	
}
}
