package HelloWorld;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class HelloWorld extends Application {

	public void start(Stage primaryStage) throws Exception {
		 primaryStage.setTitle("Hello World");
		 Button button = new Button();
		 button.setText("Hello World");
		 button.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
			       System.out.println("Hello World");
				
			}
		});
		 StackPane layOut =new StackPane();
		 layOut.getChildren().add(button);
		 Scene scene =new Scene(layOut,300,200);
		 primaryStage.setScene(scene);
		 primaryStage.show();
		
	}
	public static void main(String[] args) {
		launch(args);
	}

}
