package lab02;

import java.util.Scanner;

public class Matrix {
    public static void main(String[] args) {
		int row,col;
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap vao so dong cua ma tran: ");
		row=sc.nextInt();
		System.out.print("Nhap vao so cot cua ma tran: ");
		col=sc.nextInt();
		int matrix[][] =new int[row][col];
		 for(int i=0;i<row;i++) {
			 for(int j=0;j<col;j++) {
				 System.out.print("Nhap vao phan tu thu " +(j+1)+ " cua dong " +(i+1)+ " : ");
				 matrix[i][j]=sc.nextInt();				 
			 }
		 }
		 for(int i=0;i<row;i++) {
			 for(int j =0;j<col;j++) {
				 System.out.print(matrix[i][j]+" ");
			 }
			 System.out.println();
		 }
	}
}
