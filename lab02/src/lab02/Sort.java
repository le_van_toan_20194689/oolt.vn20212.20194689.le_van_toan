package lab02;

import java.util.Scanner;

public class Sort {
	public static void SortArr(int[] arr) {
		for(int i=0;i<arr.length;i++) {
			for(int j=i+1;j<arr.length;j++) {
				if(arr[i]>arr[j]) {
					int temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		int n;
		Scanner sc =new Scanner(System.in);
		System.out.println("Nhap vao so phan tu cua mang: ");
		n=sc.nextInt();
		int[] str=new int[n];
		int sum=0;
		for(int i=0; i<n ;i++) {
			System.out.println("Nhap vao phan tu thu "+ (i+1)+" : ");
			str[i]=sc.nextInt();
			sum +=str[i];
			}
		SortArr(str);
		for(int i=0;i<str.length;i++) {
			System.out.print(str[i]+" ");
			
		}
		System.out.println("Tong cua mang la :" +sum);
		System.out.println("Trung binh cuong cua mang la: " +(sum/n));
	}

}
