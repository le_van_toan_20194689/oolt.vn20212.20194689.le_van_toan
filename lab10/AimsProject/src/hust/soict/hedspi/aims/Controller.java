package hust.soict.hedspi.aims;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.Optional;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.order.Order;
import hust.soict.hedspi.aims.utils.MyDate;

public class Controller {
	int chosse =0;
	Order OrderOne = null;
    Media MediaItem = null;
    DigitalVideoDisc DVDItem = null;
    Book BookItem = null;
    CompactDisc CDItem=null;
   @FXML
   private Label LabelDisplay;
   @FXML
   private ImageView ExitImg;
   @FXML
   public void Display() {
	   OrderOne =new Order();
	   LabelDisplay.setText("Bạn đã tạo một Order thành công");
   }
   @FXML
   public void ChosseItem() {
	   LabelDisplay.setText("");
	   Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	   alert.setTitle("Choose Item");
	   alert.setContentText("Choose Item you want:");
	   
	   ButtonType buttonTypeDVD = new ButtonType("DVD");
	   ButtonType buttonTypeCD = new ButtonType("CD");
	   ButtonType buttonTypeBook = new ButtonType("Book");
	   
	   alert.getButtonTypes().setAll(buttonTypeDVD,buttonTypeCD,buttonTypeBook);
	   
	   Optional<ButtonType> result = alert.showAndWait();
	   if(result.get() == buttonTypeDVD ) {
		   DVDItem = new DigitalVideoDisc();
		   Stage stage1 = new Stage();
		   VBox layout =new VBox();
		   Label LabelItem =new Label();
		   TextField ItemName =new TextField();
		   Button ButtonOki= new Button();
		   LabelItem.setPadding(new Insets(10,10,10,50));
		   ButtonOki.setTranslateX(130);
		   ButtonOki.setTranslateY(10);
		   LabelItem.setText("Nhập vào tên DVD bạn muốn mua:");
		   ButtonOki.setText("OK");
		   layout.getChildren().addAll(LabelItem,ItemName,ButtonOki);
		   OrderOne.addMedia(DVDItem);
		   Scene scene =new Scene(layout,300,100);
		   stage1.setScene(scene);
		   stage1.show();
		   chosse =1;
		   ButtonOki.setOnAction(e ->{
			   DVDItem.setTitle(ItemName.getText());
			   stage1.close();
		   });
		   
	   }else if(result.get() == buttonTypeCD) {
		   CDItem = new CompactDisc();
		   Stage stage1 = new Stage();
		   VBox layout =new VBox();
		   Label LabelItem =new Label();
		   TextField ItemName =new TextField();
		   Button ButtonOki= new Button();
		   LabelItem.setPadding(new Insets(10,10,10,50));
		   ButtonOki.setTranslateX(130);
		   ButtonOki.setTranslateY(10);
		   LabelItem.setText("Nhập vào tên CD bạn muốn mua:");
		   ButtonOki.setText("OK");
		   layout.getChildren().addAll(LabelItem,ItemName,ButtonOki);
		   OrderOne.addMedia(CDItem);
		   Scene scene =new Scene(layout,300,100);
		   stage1.setScene(scene);
		   stage1.show();
		   chosse =2;
		   ButtonOki.setOnAction(e ->{
			   CDItem.setTitle(ItemName.getText());
			   stage1.close();
		   });
	   }else {
		   BookItem = new Book();
		   Stage stage1 = new Stage();
		   VBox layout =new VBox();
		   Label LabelItem =new Label();
		   TextField ItemName =new TextField();
		   Button ButtonOki= new Button();
		   LabelItem.setPadding(new Insets(10,10,10,50));
		   ButtonOki.setTranslateX(130);
		   ButtonOki.setTranslateY(10);
		   LabelItem.setText("Nhập vào tên Book bạn muốn mua:");
		   ButtonOki.setText("OK");
		   layout.getChildren().addAll(LabelItem,ItemName,ButtonOki);
		   OrderOne.addMedia(BookItem);
		   Scene scene =new Scene(layout,300,100);
		   stage1.setScene(scene);
		   stage1.show();
		   chosse =3;
		   ButtonOki.setOnAction(e ->{
			   BookItem.setTitle(ItemName.getText());
			   stage1.close();
		   });
	   }
	   LabelDisplay.setText("Ban đã thêm thành công vào giỏ hàng");
	   alert.show();
	   
   }
   @FXML
   public void DeleteItem() {
	   if(chosse ==1) {
		   OrderOne.removeMedia(DVDItem);
	   }else if(chosse ==2) {
		   OrderOne.removeMedia(CDItem);
	   }else {
		   OrderOne.removeMedia(BookItem);
	   }
	   LabelDisplay.setText("Bạn đã xóa mặt hàng bạn vừa thêm vào");
   }
   @FXML
   public void DisplayItem() {
	    Stage StageDisplay = new Stage();
	    ListView mediaListView = new ListView();
	    for(Media media : OrderOne.getItemOrdered()){
	    	mediaListView.getItems().add(media.getTitle());
	    }
	    Label LabelItem = new Label();
	    LabelItem.setText("Danh sách Item của bạn như sau: ");
	    LabelItem.setFont(new Font("Arial",12));
	    LabelItem.setTextAlignment(TextAlignment.CENTER);
	    VBox layout = new VBox();
	    layout.setPadding(new Insets(10,10,10,10));
	    layout.getChildren().addAll(LabelItem,mediaListView);
	    StageDisplay.setScene(new Scene(layout,300,300));
	    StageDisplay.show();
	    
   }
   @FXML
   public void Exit() {
	   try {
		   Platform.exit();
	       System.exit(0);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
   }
   
  
}
