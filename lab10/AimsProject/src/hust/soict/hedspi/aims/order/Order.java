package hust.soict.hedspi.aims.order;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import java.util.ArrayList;
import java.util.List;
public class Order {
	
	
     public static final int MAX_NUMBERS_ORDERED = 10;
     private List<Media> itemOrdered = new ArrayList <Media>();
     private int qtyOrdered =0;
//   Them dateOrderd
     public static final int MAX_LIMITTED_ORDERS =5;
     private static int nbOrders=0;
     
     private Date dateOrderd;
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}



	public List<Media> getItemOrdered() {
		return itemOrdered;
	}

	public void setItemOrdered(List<Media> itemOrdered) {
		this.itemOrdered = itemOrdered;
	}

	public Date getDateOrderd() {
		return dateOrderd;
	}

	public void setDateOrderd(Date dateOrderd) {
		this.dateOrderd = dateOrderd;
	}

	public void addMedia(Media mediaName) {
		if(!(this.itemOrdered.contains(mediaName))) {
			this.itemOrdered.add(mediaName);
		}
	}
	
	public void removeMedia(Media mediaName) {
		if(this.itemOrdered.contains(mediaName)) {
			this.itemOrdered.remove(mediaName);
		}
	}
	
	// Constructor Order moi khi tao Order thi nbOrders se dcc cap nhat
	public Order() {
		if(nbOrders == MAX_LIMITTED_ORDERS) {
			System.out.println("Order da day khong the them vao mat hang moi");
		}else {
		// TODO Auto-generated constructor stub
		nbOrders++;
		//       Them gia tri cho dateOrdered
	       long millis=System.currentTimeMillis();
	        this.dateOrderd= new Date(millis);
	}
}

	
	public float totalCost() {
		float total=0;
		for(Media media : itemOrdered) {
			total +=media.getCost();
			}
		
		return total;
	}
 //In ra mat hang cua tung orderd nguoi dung
    public void printOrders() {
    	System.out.println("Date: " + this.dateOrderd);
    	System.out.println("Ordered Items:");
    	for(Media media : itemOrdered) {
    	       System.out.println(""+media.getTitle());
    	}
    	System.out.println("Total cost:"+this.totalCost());

    }


// Phuong thuc getALuckyItem()

//   public Media getALuckyItem(){
//	   int i = (int) Math.random();
//       int rand = (int)( i * this.itemOrdered.size());
////       System.out.println(rand);
//       itemOrdered.get(rand).setCost(rand);
//      return itemOrdered.get(rand);
//          
//       
//   }


}
 



