package hust.soict.hedspi.aims.media;
import java.util.Objects;
import java.util.Scanner;

public class DigitalVideoDisc extends Disc implements Playable  {
   
// Contructor ko tham so
   public DigitalVideoDisc(){
	  
	   this.title="";
	   this.category="";
	   this.director="";
	   this.length=0;
	   this.cost=0.0f;
   }

public DigitalVideoDisc(String title) {
	super();
	this.title = title;
}

// Contructor co full tham so   
public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
	super();
	this.title = title;
	this.category = category;
	this.director = director;
	this.length = length;
	this.cost = cost;
}

// Ham so sanh hai class khac nhauu voi nhau 
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	DigitalVideoDisc other = (DigitalVideoDisc) obj;
	return Objects.equals(category, other.category) && Float.floatToIntBits(cost) == Float.floatToIntBits(other.cost)
			&& Objects.equals(director, other.director) && length == other.length && Objects.equals(title, other.title);
}

// Phuong thuc getter and setter 

public String getDirector() {
	return director;
}
public void setDirector(String director) {
	this.director = director;
}
public int getLength() {
	return length;
}
public void setLength(int length) {
	if(length >0)
		this.length =length;
	else this.length = 0;
}

public void print() {
	System.out.println("DVD-"+this.getTitle()+"-"
            +this.getCategory()+"-"
	        +this.getDirector()+"-"
	        +this.getLength()+":"
            +this.getCost());	
}

// Phuong thuc Search
public boolean Search (String title ) {
	String sTitle[] = title.split(" ");
	String strTitle[]=this.title.split(" ");
	int count=0;
	for(int i=0;i<sTitle.length;i++) {
		for(int j=0;j<strTitle.length;j++) {
		     if(sTitle[i].equalsIgnoreCase(strTitle[j])) {
		    	 count++;
		    	 break;
		     }
		}
	}
	if(count == sTitle.length) return true;
	return false;
}

@Override
public void play() {
	System.out.println("Playing DVD: " + this.getTitle());
	System.out.println("DVD Category: " + this.getCategory());
	System.out.println("DVD Director: " + this.getDirector());
	System.out.println("DVD Length: " + this.getLength());
	System.out.println("DVD Cost: " +this.getCost());
	
}

}