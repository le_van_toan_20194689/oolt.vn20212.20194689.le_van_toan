package hust.soict.hedspi.aims.media;

public class Track implements Playable,Comparable {
     private String title;
     private int length;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public Track(String title, int length) {
		this.title = title;
		this.length = length;
	}
//	override play
	public void play() {
		System.out.println("Playing DVD: "+ this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
    public boolean equals(Object o) {
    	Track track1 = (Track) o;
    	if(this.title.equals(track1.title) && this.length == track1.length) {
    		return true;
    	}else {
    		return false;
    	}
    }
	@Override
	public int CompareTo(Object o) {
		// TODO Auto-generated method stub
		Track track1 = (Track) o;
		return this.title.compareTo(track1.getTitle());
	}
     
	

}
