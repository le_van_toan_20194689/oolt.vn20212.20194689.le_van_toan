package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Book extends Media{
  
   private List<String> authors =new ArrayList<String>();
   private String content;
   private List<String> contentTokens;
   private Map<String, Integer> wordFrequency;
   
   public Book() {
	  
	   this.title ="";
   }
   public Book(String title) {
	   super(title);
   }
   public Book(String title, String category) {
	   super(title,category);
   }
   public Book (String title , String category, List<String> authors) {
	   super(title,category);
	   this.authors=authors;
   }
   public void addAuthor(String authorName) {
	   if(!(authors.contains(authorName))) {
		   authors.add(authorName);
	   }
   }
   public List<String> getAuthors() {
	return authors;
   }
   public void setAuthors(List<String> authors) {
	this.authors = authors;
    }
   public void setContent(String content) {
	   		this.content = content;
	   		this.processContent();
	   	}
	   	public String getContent() {
	   		return this.content;
	   	}
   public void print() {
		System.out.println("Book-"+this.getTitle()+"-"
	            +this.getCategory()+"-"
		        +this.getAuthors()+"-"
	            +this.getCost());	
	}
   public void removeAuthor(String authorName) {
	   if(authors.contains(authorName)) {
		   authors.remove(authorName);
	   }
   }
   public void processContent() {
	   		// split by punctuations
	   		contentTokens = new ArrayList<String>();
	   		String[] temp =  content.split("[\\p{Punct}\\s]+");
	   		Arrays.sort(temp);
	   		contentTokens.addAll(Arrays.asList(temp));
	   		
	   		// Count the frequency of each token
	   		wordFrequency = new TreeMap<String, Integer>();
	   		String pre = "";
	   		int count = 0;
	   		for (String token : contentTokens) {
	   			if (token.compareTo(pre) != 0) {
	   				if (count > 0) {
	   					wordFrequency.put(pre, count);
	   				}
	   			} else {
	   				count++;
	   			}
	   			pre = token;
	   		}
	   		if (count > 0) {
	   			wordFrequency.put(pre, count);
	   		}
	   	}
	   	
	   	public String toString() {
	   		String res = "Title: " + title + "\n"
	   				+ "Category: " + category + "\n"
	   				+ "Cost: " + cost + "\n"
	   				+ "Authors:";
	   		for (String author:authors) {
	   			res+= " " + author;
	   		}
	   		res+= "\nContent length: " + contentTokens.size() + "\n";
	   		
	   		for (Map.Entry<String, Integer> word : wordFrequency.entrySet()) {
	   			res+= word.getKey() + "-" + word.getValue() + "\n";
	   		}
	   		return res;
	   	}
}