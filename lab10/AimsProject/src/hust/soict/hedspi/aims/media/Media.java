package hust.soict.hedspi.aims.media;

public abstract class Media implements Comparable {
	protected int id;
	protected String title;
    protected String category;
    protected float cost;
	public Media() {
		// TODO Auto-generated constructor stub
	}
	public Media(String title) {
		this.title =title;
	}
	public Media(String title,String category) {
		this(title);
		this.category=category;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public float getCost() {
		return cost;
	}
	public boolean epuals(Object o) {
		Media media1 =(Media) o;
		if (this.id == media1.id) {
			return true;
		}else {
			return false;
		}
		
	}
    public int CompareTo(Object o) {
    	Media media1 = (Media) o;
    	return this.title.compareTo(media1.getTitle());
    }
    
}
