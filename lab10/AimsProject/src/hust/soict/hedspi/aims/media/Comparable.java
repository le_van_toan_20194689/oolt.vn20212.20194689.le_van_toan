package hust.soict.hedspi.aims.media;

public interface Comparable {
    public int CompareTo(Object o);
}
