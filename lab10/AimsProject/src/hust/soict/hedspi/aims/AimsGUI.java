package hust.soict.hedspi.aims;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AimsGUI extends Application {

	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("AimsGUI.fxml"));
			primaryStage.setTitle("Amis Order");
			primaryStage.setScene(new Scene(root,669,550));
			 primaryStage.setOnCloseRequest(e -> {
			        Platform.exit();
			        System.exit(0);
			    });
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
