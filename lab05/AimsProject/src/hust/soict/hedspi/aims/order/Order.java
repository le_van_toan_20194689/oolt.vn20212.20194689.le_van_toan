package hust.soict.hedspi.aims.order;
import java.sql.Date;

import hust.soict.hedspi.aims.disc.DigitalVideoDisc;

public class Order {
	
	
     public static final int MAX_NUMBERS_ORDERED = 10;
     private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
     private int qtyOrdered =0;
//   Them dateOrderd
     public static final int MAX_LIMITTED_ORDERS =5;
     private static int nbOrders=0;
     
     private Date dateOrderd;
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}

	public DigitalVideoDisc[] getItemOrdered() {
		return itemOrdered;
	}

	public void setItemOrdered(DigitalVideoDisc[] itemOrdered) {
		this.itemOrdered = itemOrdered;
	}

	public Date getDateOrderd() {
		return dateOrderd;
	}

	public void setDateOrderd(Date dateOrderd) {
		this.dateOrderd = dateOrderd;
	}

	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
       if(this.qtyOrdered == MAX_NUMBERS_ORDERED) {
    	   System.out.println("Thee order is almost full");
       }else {
    	   this.itemOrdered[qtyOrdered]=disc;
    	   qtyOrdered++;
       }
       long millis=System.currentTimeMillis();
		//       Them gia tri cho dateOrdered
        this.dateOrderd= new Date(millis);
	}
	
	// Constructor Order moi khi tao Order thi nbOrders se dcc cap nhat
	public Order() {
		if(nbOrders == MAX_LIMITTED_ORDERS) {
			System.out.println("Order da day khong the them vao mat hang moi");
		}else {
		// TODO Auto-generated constructor stub
		nbOrders++;
		//       Them gia tri cho dateOrdered
	       long millis=System.currentTimeMillis();
	        this.dateOrderd= new Date(millis);
	}
}
	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdlist) {
		if((this.qtyOrdered + dvdlist.length)>= MAX_NUMBERS_ORDERED) {
			System.out.println("Khong the them vao gio hang vi qua so luong");
		}else {
			for(int i=0;i<dvdlist.length;i++) {
				this.itemOrdered[qtyOrdered]=dvdlist[i];
				qtyOrdered++;
			}
		}

	}
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2) {
		if(this.qtyOrdered >= MAX_NUMBERS_ORDERED-1) {
	    	   System.out.println("Thee order is almost full");
	       }else {
	    	   this.itemOrdered[qtyOrdered]=dvd1;
	    	   this.itemOrdered[qtyOrdered+1]=dvd2;
	    	   qtyOrdered+=2;
	       }
	      
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc  disc) {
		int c,i;
		for (c=i=0;i<this.qtyOrdered;i++) {
			if(this.itemOrdered[i].equals(disc) != true) {
				this.itemOrdered[c]=this.itemOrdered[i];
				c++;
			}
		}
		this.qtyOrdered--;
	}
	public float totalCost() {
		float total=0;
		for(int i=0 ; i<qtyOrdered;i++) {
			total +=itemOrdered[i].getCost();
			}
		return total;
	}
// In ra mat hang cua tung orderd nguoi dung
    public void printOrders() {
    	System.out.println("Date: " + this.dateOrderd);
    	System.out.println("Ordered Items:");
    	for(int i=0;i<this.qtyOrdered;i++) {
    	       this.itemOrdered[i].printDVD();
    	}
    	System.out.println("Total cost:"+this.totalCost());

    }

// Phuong thuc getALuckyItem()

   public DigitalVideoDisc getALuckyItem(){
	   int i = (int) Math.random();
       int rand = (int)( i * this.itemOrdered.length);
//       System.out.println(rand);
       this.itemOrdered[rand].setCost(0);
      return this.itemOrdered[rand];
          
       
   }
}
 



