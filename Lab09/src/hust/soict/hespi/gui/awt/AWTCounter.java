package hust.soict.hespi.gui.awt;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class AWTCounter extends Frame implements ActionListener {
	private Label lblCount;
	private TextField tfCount;
	private Button btnCount;
	private int count;
	
	public AWTCounter() {
//		Thiet lap layout cua so
		this.setLayout(new FlowLayout());
		
		
		lblCount = new Label("Counter");
//		Them vao cua so giao dien
		this.add(lblCount);
		
		tfCount = new TextField(count + "" ,10);
		tfCount.setEditable(false);
		this.add(tfCount);
		
		btnCount = new Button("Count");
		this.add(btnCount);
//      Dang ky lang nghe 
		btnCount.addActionListener( this);
		addWindowListener(new MyWindowListener());
//		thiet lap thong tin cua so giao dien
		this.setTitle("AWT Counter");
		this.setSize(250, 100);
		this.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e) {
		++count;
		this.tfCount.setText(count +"");
	}
	public static void main(String[] args) {
		AWTCounter app = new AWTCounter();
		

	}
	   private class MyWindowListener implements WindowListener {
		      // Called back upon clicking close-window button
		      @Override
		      public void windowClosing(WindowEvent evt) {
		         System.exit(0);  // Terminate the program
		      }

		      // Not Used, BUT need to provide an empty body to compile.
		      @Override public void windowOpened(WindowEvent evt) { }
		      @Override public void windowClosed(WindowEvent evt) { }
		      // For Debugging
		      @Override public void windowIconified(WindowEvent evt) { System.out.println("Window Iconified"); }
		      @Override public void windowDeiconified(WindowEvent evt) { System.out.println("Window Deiconified"); }
		      @Override public void windowActivated(WindowEvent evt) { System.out.println("Window Activated"); }
		      @Override public void windowDeactivated(WindowEvent evt) { System.out.println("Window Deactivated"); }
		   }

}
